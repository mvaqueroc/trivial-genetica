
public class Preguntas {
	private int pregunta_id;
	private String enunciado;
	private String respuesta_1;
	private String respuesta_2;
	private String respuesta_3;
	private int correcta;
	private int temas_temas_id;

	public Preguntas(int pregunta_id, String enunciado, String respuesta_1, String respuesta_2, String respuesta_3,
			int correcta, int temas_temas_id) {
		this.pregunta_id = pregunta_id;
		this.enunciado = enunciado;
		this.respuesta_1 = respuesta_1;
		this.respuesta_2 = respuesta_2;
		this.respuesta_3 = respuesta_3;
		this.correcta = correcta;
		this.temas_temas_id = temas_temas_id;
	}

	public Preguntas() {
	}

	public int getPregunta_id() {
		return pregunta_id;
	}

	public void setPregunta_id(int pregunta_id) {
		this.pregunta_id = pregunta_id;
	}

	public String getEnunciado() {
		return enunciado;
	}

	public void setEnunciado(String enunciado) {
		this.enunciado = enunciado;
	}

	public String getRespuesta_1() {
		return respuesta_1;
	}

	public void setRespuesta_1(String respuesta_1) {
		this.respuesta_1 = respuesta_1;
	}

	public String getRespuesta_2() {
		return respuesta_2;
	}

	public void setRespuesta_2(String respuesta_2) {
		this.respuesta_2 = respuesta_2;
	}

	public String getRespuesta_3() {
		return respuesta_3;
	}

	public void setRespuesta_3(String respuesta_3) {
		this.respuesta_3 = respuesta_3;
	}

	public int getCorrecta() {
		return correcta;
	}

	public void setCorrecta(int correcta) {
		this.correcta = correcta;
	}

	public int getTemas_temas_id() {
		return temas_temas_id;
	}

	public void setTemas_temas_id(int temas_temas_id) {
		this.temas_temas_id = temas_temas_id;
	}

}
