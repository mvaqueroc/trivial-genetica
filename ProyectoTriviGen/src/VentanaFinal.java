
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class VentanaFinal extends JFrame {

	private JPanel contentPane;
	private Preguntas pregunta;
	boolean contestado = false;
	private JButton button;
	private int correcta;
	private static final String tema1 = "Herencia y Reproduccion";
	private static final String tema2 = "Conceptos Generales";
	private static final String tema3 = "Enfermedades y Terapias";
	private static final String tema4 = "Aplicaciones de la Genetica";
	private String[] temasList = new String[] { tema1, tema2, tema3, tema4 };
	
	
	

	public VentanaFinal(int contador, Jugador jugadoractual, Inicio frameinicio, Tablero frametablero) {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 750, 410);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[:740px:740px]",
				"[:40px:40px][10px][:100px:100px][30px][40px][20px][40px][20px][40px][20px][40px]"));
		pregunta = DBConex.leePregunta(temasList[contador]);
		correcta = pregunta.getCorrecta();

		JLabel temacasilla = new JLabel();
		temacasilla.setFont(new Font("Georgia", Font.BOLD, 11));
		contentPane.add(temacasilla, "cell 0 0,alignx center,growy");
		temacasilla.setText(temasList[contador].toString());

		JTextArea enunciado = new JTextArea("");
		enunciado.setRows(6);
		enunciado.setWrapStyleWord(true);
		enunciado.setLineWrap(true);
		enunciado.setEditable(false);
		enunciado.setFont(new Font("Georgia", Font.PLAIN, 15));
		contentPane.add(enunciado, "cell 0 2,grow");
		enunciado.setText(pregunta.getEnunciado());

		JButton resp1 = new JButton("");
		resp1.setFont(new Font("Georgia", Font.PLAIN, 13));
		resp1.setText(pregunta.getRespuesta_1());
		resp1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (contestado == false) {
					resp1.setBackground(new Color(64, 224, 208));
					int boton = 1;
					if (boton == correcta) {
						Tablero.contadorAciertos++;
					}
					contestado = true;
					button.setText("Siguiente");
					button.setOpaque(true);
					button.setContentAreaFilled(true);
					button.setBorderPainted(true);

				}
			}
		});

		contentPane.add(resp1, "cell 0 4");

		JButton resp2 = new JButton("");
		resp2.setText(pregunta.getRespuesta_2());
		resp2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (contestado == false) {
					resp2.setBackground(new Color(64, 224, 208));
					int boton = 2;
					if (boton == correcta) {
						Tablero.contadorAciertos++;

					}
					contestado = true;
					button.setText("Siguiente");
					button.setOpaque(true);
					button.setContentAreaFilled(true);
					button.setBorderPainted(true);
				}

			}
		});
		resp2.setFont(new Font("Georgia", Font.PLAIN, 13));
		contentPane.add(resp2, "cell 0 6");

		JButton resp3 = new JButton("");
		resp3.setText(pregunta.getRespuesta_3());
		resp3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (contestado == false) {
					resp3.setBackground(new Color(64, 224, 208));
					int boton = 3;
					if (boton == correcta) {
						Tablero.contadorAciertos++;
					}
					contestado = true;

					button.setText("Siguiente");
					button.setOpaque(true);
					button.setContentAreaFilled(true);
					button.setBorderPainted(true);
				}
			}
		});
		resp3.setFont(new Font("Georgia", Font.PLAIN, 13));
		contentPane.add(resp3, "cell 0 8");

		button = new JButton("");
		button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (contestado == true && Tablero.contadorFinal < 3) {
					Tablero.contadorFinal++;
					VentanaFinal siguientepag = new VentanaFinal(Tablero.contadorFinal, jugadoractual, frameinicio,
							frametablero);
					siguientepag.setVisible(true);
					siguientepag.setLocationRelativeTo(null);
					dispose();
				} else {
					if (Tablero.contadorAciertos >= 3) {
						VentanaGanador resultpag = new VentanaGanador(Tablero.contadorAciertos, jugadoractual,
								frameinicio, frametablero);
						resultpag.setVisible(true);
						resultpag.setLocationRelativeTo(null);
						dispose();
					} else {
						VentanaPerdedor resultpag1 = new VentanaPerdedor(Tablero.contadorAciertos, jugadoractual);
						resultpag1.setVisible(true);
						resultpag1.setLocationRelativeTo(null);
						dispose();
					}
				}
			}
		});
		contentPane.add(button, "cell 0 10,alignx right");
	}
}