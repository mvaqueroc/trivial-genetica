
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class VentanaPreg extends JFrame {

	private JPanel contentPane;
	private Preguntas pregunta;
	boolean contestado = false;
	boolean acierto = false;
	private JButton button;

	public VentanaPreg(String tema, Casilla casi, Jugador juga, int numjugadores, int jugadoractual) {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 750, 410);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[:740px:740px]",
				"[:40px:40px][10px][:80px:80px][30px][40px][20px][40px][20px][40px][20px][40px]"));
		pregunta = DBConex.leePregunta(tema);

		JLabel temacasilla = new JLabel();
		temacasilla.setFont(new Font("Georgia", Font.BOLD, 15));
		contentPane.add(temacasilla, "cell 0 0,alignx center,growy");
		temacasilla.setText(tema);

		JButton resp1 = new JButton("");
		resp1.setFont(new Font("Georgia", Font.PLAIN, 13));
		resp1.setText(pregunta.getRespuesta_1());
		resp1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (contestado == false) {
					int boton = 1;

					if (boton == pregunta.getCorrecta()) {
						acierto = true;
						resp1.setBackground(new Color(144, 238, 144));
					} else {
						resp1.setBackground(new Color(250, 128, 114));
					}
					contestado = true;
					button.setText("Continuar");
					button.setOpaque(true);
					button.setContentAreaFilled(true);
					button.setBorderPainted(true);
				}
			}
		});

		JTextArea enunciado = new JTextArea("");
		enunciado.setRows(6);
		enunciado.setWrapStyleWord(true);
		enunciado.setLineWrap(true);
		enunciado.setEditable(false);
		enunciado.setFont(new Font("Georgia", Font.PLAIN, 15));
		contentPane.add(enunciado, "cell 0 2,grow");
		enunciado.setText(pregunta.getEnunciado());

		contentPane.add(resp1, "cell 0 4");

		JButton resp2 = new JButton("");
		resp2.setText(pregunta.getRespuesta_2());
		resp2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (contestado == false) {

					int boton = 2;
					if (boton == pregunta.getCorrecta()) {
						acierto = true;
						resp2.setBackground(new Color(144, 238, 144));
					} else {
						resp2.setBackground(new Color(250, 128, 114));
					}
					contestado = true;
					button.setText("Continuar");
					button.setOpaque(true);
					button.setContentAreaFilled(true);
					button.setBorderPainted(true);
				}

			}
		});
		resp2.setFont(new Font("Georgia", Font.PLAIN, 13));
		contentPane.add(resp2, "cell 0 6");

		JButton resp3 = new JButton("");
		resp3.setText(pregunta.getRespuesta_3());
		resp3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (contestado == false) {
					int boton = 3;
					if (boton == pregunta.getCorrecta()) {
						acierto = true;
						resp3.setBackground(new Color(144, 238, 144));
					} else {
						resp3.setBackground(new Color(250, 128, 114));
					}
					contestado = true;
					button.setText("Continuar");
					button.setOpaque(true);
					button.setContentAreaFilled(true);
					button.setBorderPainted(true);
				}
			}
		});
		resp3.setFont(new Font("Georgia", Font.PLAIN, 13));

		contentPane.add(resp3, "cell 0 8");

		button = new JButton("");
		button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (contestado == true) {

					if (casi.getTipo() == 2 && acierto == true) {
						Tablero.pintaQuesito(casi.getTema(), juga);
					}

					Tablero.actualizaturno(acierto);
					Tablero.actualizaturnojugador(Tablero.jugadoractual);
					Tablero.actualizaquesito(Tablero.jugadoractual);
					dispose();
				}
			}
		});
		
		contentPane.add(button, "cell 0 10,alignx right");
	}
}
