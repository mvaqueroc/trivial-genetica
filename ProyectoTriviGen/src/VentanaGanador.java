
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import java.awt.Color;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

@SuppressWarnings("serial")
public class VentanaGanador extends JFrame {

	private JPanel contentPane;
	public Inicio frameinicio;
	
	
	

	/**
	 * Create the frame.
	 */
	public VentanaGanador(int aciertos, Jugador jugador, Inicio frameinicio, Tablero frametablero) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 606, 459);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("FELICIDADES");
		lblNewLabel.setBounds(189, 62, 207, 33);
		lblNewLabel.setFont(new Font("Georgia", Font.BOLD, 28));
		lblNewLabel.setForeground(new Color(231, 174, 24));
		contentPane.add(lblNewLabel);

		JButton btnNewButton = new JButton("Volver al menu");
		btnNewButton.setBounds(452, 388, 117, 29);
		btnNewButton.setFont(new Font("Georgia", Font.PLAIN, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Tablero.contadorFinal = 0;
				Tablero.contadorAciertos = 0;
				frameinicio.setVisible(true);
				frametablero.dispose();
				dispose();
			}

		});
		JLabel lblNewLabel_2 = new JLabel(jugador.getNombre());
		lblNewLabel_2.setBounds(214, 116, 207, 28);
		lblNewLabel_2.setForeground(new Color(231, 174, 24));
		lblNewLabel_2.setFont(new Font("Georgia", Font.BOLD, 24));
		contentPane.add(lblNewLabel_2);

		JLabel lblHasAcertado = new JLabel("Has acertado " + aciertos + "/4");
		lblHasAcertado.setBounds(214, 208, 142, 28);
		lblHasAcertado.setFont(new Font("Georgia", Font.BOLD, 15));
		contentPane.add(lblHasAcertado);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(VentanaGanador.class.getResource("/dado/Recurso 34.png")));
		lblNewLabel_1.setBounds(40, 6, 500, 391);
		contentPane.add(lblNewLabel_1);

	}
}
