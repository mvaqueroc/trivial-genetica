
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class Jugadores3 extends JFrame {

	private static final int MAX_CHAR = 25;
	private JPanel contentPane;
	private JTextField textField_Player1;
	public Inicio frameinicio;
	private JTextField textField_Player2;
	private JTextField textField_Player3;

	/**
	 * Create the frame.
	 */

	public Jugadores3() {
		inicioComponentes();
	}

	public Jugadores3(Inicio ini) {
		this.frameinicio = ini;
		inicioComponentes();
	}

	private void inicioComponentes() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 253);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(
				new MigLayout("", "[65.00][grow][162.00,grow][120.00][]", "[32][32px][32px][32px][20px][32px]"));

		JLabel lblPlayer1 = new JLabel("Jugador 1");
		lblPlayer1.setFont(new Font("Georgia", Font.PLAIN, 11));
		contentPane.add(lblPlayer1, "cell 0 1,alignx trailing");

		textField_Player1 = new JTextField();
		textField_Player1.setFont(new Font("Georgia", Font.PLAIN, 11));
		textField_Player1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (textField_Player1.getText().length() > MAX_CHAR) {
					e.consume();
				}
			}
		});
		contentPane.add(textField_Player1, "cell 1 1 2 1,growx");
		textField_Player1.setColumns(10);

		JLabel lblPlayer2 = new JLabel("Jugador 2");
		lblPlayer2.setFont(new Font("Georgia", Font.PLAIN, 11));
		contentPane.add(lblPlayer2, "cell 0 2,alignx trailing");

		textField_Player2 = new JTextField();
		textField_Player2.setFont(new Font("Georgia", Font.PLAIN, 11));
		textField_Player2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (textField_Player2.getText().length() > MAX_CHAR) {
					e.consume();
				}
			}
		});
		contentPane.add(textField_Player2, "cell 1 2 2 1,growx");
		textField_Player2.setColumns(10);

		JLabel lblPlayer3 = new JLabel("Jugador 3");
		lblPlayer3.setFont(new Font("Georgia", Font.PLAIN, 11));
		contentPane.add(lblPlayer3, "cell 0 3,alignx trailing");

		textField_Player3 = new JTextField();
		textField_Player3.setFont(new Font("Georgia", Font.PLAIN, 11));
		textField_Player3.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (textField_Player3.getText().length() > MAX_CHAR) {
					e.consume();
				}
			}
		});
		contentPane.add(textField_Player3, "cell 1 3 2 1,growx");
		textField_Player3.setColumns(10);

		JButton button = new JButton("Atr�s");
		button.setFont(new Font("Georgia", Font.PLAIN, 11));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frameinicio.setVisible(true);
				dispose();
			}
		});
		contentPane.add(button, "cell 1 5");

		JButton btnPlay = new JButton("Jugar");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Tablero pantablero = new Tablero(frameinicio, textField_Player1.getText(), textField_Player2.getText(),
						textField_Player3.getText());
				pantablero.setVisible(true);
				pantablero.setLocationRelativeTo(null);
				dispose();
			}
		});
		btnPlay.setFont(new Font("Georgia", Font.PLAIN, 11));
		contentPane.add(btnPlay, "cell 3 5");
	}

}
