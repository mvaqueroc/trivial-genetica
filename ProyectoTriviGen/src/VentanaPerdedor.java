
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

@SuppressWarnings("serial")
public class VentanaPerdedor extends JFrame {

	private JPanel contentPane;
	public Inicio frameinicio;

	public VentanaPerdedor(int aciertos, Jugador jugador) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 609, 403);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Oh no!");
		lblNewLabel.setBounds(247, 44, 73, 23);
		lblNewLabel.setFont(new Font("Georgia", Font.BOLD, 28));
		lblNewLabel.setForeground(new Color(185, 184, 181));
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_2 = new JLabel("" + aciertos + "/4");
		lblNewLabel_2.setBounds(258, 193, 44, 28);
		lblNewLabel_2.setForeground(new Color(0, 0, 0));
		lblNewLabel_2.setFont(new Font("Georgia", Font.BOLD, 24));
		contentPane.add(lblNewLabel_2);

		JButton btnNewButton = new JButton("Sigue jugando");
		btnNewButton.setBounds(476, 334, 112, 29);
		btnNewButton.setFont(new Font("Georgia", Font.PLAIN, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Tablero.contadorFinal = 0;
				Tablero.contadorAciertos = 0;
				Tablero.actualizaturno(false);
				Tablero.actualizaturnojugador(Tablero.jugadoractual);
				Tablero.actualizaquesito(Tablero.jugadoractual);
				dispose();
			}
		});
		
		JLabel label = new JLabel(jugador.getNombre());
		label.setForeground(new Color(185, 184, 181));
		label.setFont(new Font("Georgia", Font.BOLD, 24));
		label.setBounds(218, 92, 162, 28);
		contentPane.add(label);

		
		JLabel lblSoloHasAcertado = new JLabel("Solo has acertado:");
		lblSoloHasAcertado.setFont(new Font("Georgia", Font.BOLD, 15));
		lblSoloHasAcertado.setBounds(201, 165, 162, 16);
		contentPane.add(lblSoloHasAcertado);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(VentanaPerdedor.class.getResource("/dado/Recurso 35.png")));
		lblNewLabel_1.setBounds(42, 6, 510, 369);
		contentPane.add(lblNewLabel_1);
		contentPane.add(btnNewButton);
	}
}