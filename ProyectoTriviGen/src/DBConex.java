
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.jdbc.Connection;

public class DBConex {

	
	
	
	private static final String URL = "jdbc:mysql://localhost:3306/trivigen";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "  ";
//	private static final String URL = "jdbc:mysql://localhost:3306/trivigen";
//	private static final String USERNAME = "root";
//	private static final String PASSWORD = "  ";
	private static final Logger LOG = Logger.getLogger(DBConex.class.getName());

	public static ArrayList<Preguntas> leePreguntas(String tema) {

		LOG.info("INICIO carga preguntas");
		ArrayList<Preguntas> preguntaList = new ArrayList<Preguntas>();

		try (Connection conn = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(
					"select pregunta_id, enunciado, respuesta_1, respuesta_2, respuesta_3, correcta, temas_id, nombre "
							+ "from preguntas p left join temas t on t.temas_id = p.temas_temas_id where nombre = '"
							+ tema + "';");

			while (rs.next()) {
				preguntaList.add(new Preguntas((Integer) rs.getObject(1), (String) rs.getObject(2),
						(String) rs.getObject(3), (String) rs.getObject(4), (String) rs.getObject(5),
						(Integer) rs.getObject(6), (Integer) rs.getObject(7)));
			}

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error cargando preguntas", e);
		}

		LOG.info("INICIO carga preguntas");
		return preguntaList;

	}

	public static Preguntas leePregunta(String tema) {

		LOG.info("INICIO carga preguntas");
		Preguntas preguntaselec = new Preguntas();

		try (Connection conn = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
				Statement stmt = conn.createStatement()) {

			StringBuilder sb = new StringBuilder();

			sb.append(
					"select pregunta_id, enunciado, respuesta_1, respuesta_2, respuesta_3, correcta, temas_id, nombre from preguntas p left join temas t on t.temas_id = p.temas_temas_id where nombre = '");
			sb.append(tema).append("' order by rand() limit 1;");

			ResultSet rs = stmt.executeQuery(sb.toString());

			while (rs.next()) {
				preguntaselec = new Preguntas((Integer) rs.getObject(1), (String) rs.getObject(2),
						(String) rs.getObject(3), (String) rs.getObject(4), (String) rs.getObject(5),
						(Integer) rs.getObject(6), (Integer) rs.getObject(7));
			}

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error cargando preguntas", e);
		}

		LOG.info("INICIO carga preguntas");
		return preguntaselec;

	}

}
