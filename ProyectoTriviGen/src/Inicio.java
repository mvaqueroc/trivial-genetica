
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Dimension;

@SuppressWarnings("serial")
public class Inicio extends JFrame {

	private JPanel contentPane;

	public Inicio() {

		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 322, 404);
		contentPane = new JPanel();

		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setPreferredSize(new Dimension(322, 404));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnNewButton = new JButton("Un Jugador");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Jugadores1 panjuga1 = new Jugadores1(Inicio.this);
				panjuga1.setVisible(true);
				panjuga1.setLocationRelativeTo(null);
				Inicio.this.setVisible(false);

			}
		});
		btnNewButton.setFont(new Font("Georgia", Font.PLAIN, 13));
		btnNewButton.setBounds(6, 42, 307, 51);
		contentPane.add(btnNewButton);

		JButton btnDosJugadores = new JButton("Dos Jugadores");
		btnDosJugadores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Jugadores2 panjuga2 = new Jugadores2(Inicio.this);
				panjuga2.setVisible(true);
				panjuga2.setLocationRelativeTo(null);
				Inicio.this.setVisible(false);

			}
		});
		btnDosJugadores.setFont(new Font("Georgia", Font.PLAIN, 13));
		btnDosJugadores.setBounds(6, 105, 307, 51);
		contentPane.add(btnDosJugadores);

		JButton btnTresJugadores = new JButton("Tres Jugadores");
		btnTresJugadores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Jugadores3 panjuga3 = new Jugadores3(Inicio.this);
				panjuga3.setVisible(true);
				panjuga3.setLocationRelativeTo(null);
				Inicio.this.setVisible(false);
			}
		});
		btnTresJugadores.setFont(new Font("Georgia", Font.PLAIN, 13));
		btnTresJugadores.setBounds(6, 168, 307, 51);
		contentPane.add(btnTresJugadores);

		JButton btnCuatroJugadores = new JButton("Cuatro Jugadores");
		btnCuatroJugadores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Jugadores4 panjuga4 = new Jugadores4(Inicio.this);
				panjuga4.setVisible(true);
				panjuga4.setLocationRelativeTo(null);
				Inicio.this.setVisible(false);
			}
		});
		btnCuatroJugadores.setFont(new Font("Georgia", Font.PLAIN, 13));
		btnCuatroJugadores.setBounds(6, 228, 307, 51);
		contentPane.add(btnCuatroJugadores);

		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnSalir.setFont(new Font("Georgia", Font.PLAIN, 13));
		btnSalir.setBounds(72, 291, 175, 51);
		contentPane.add(btnSalir);

		JLabel lblNewLabel = new JLabel("TriviGen");
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setFont(new Font("Georgia", Font.PLAIN, 16));
		lblNewLabel.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel.setBounds(122, 14, 101, 16);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("made by: XY x3  XX x2");
		lblNewLabel_1.setBounds(6, 354, 147, 16);
		contentPane.add(lblNewLabel_1);

		JLabel lblOpenSource = new JLabel("Open Source");
		lblOpenSource.setBounds(231, 354, 82, 16);
		contentPane.add(lblOpenSource);
	}
}