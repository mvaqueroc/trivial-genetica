
import java.util.ArrayList;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class Casilla extends JButton {

	private String tema;
	private int indice;
	private ArrayList<Casilla> colindantes;
	private int tipo;
	private boolean tienejugador1;
	private boolean tienejugador2;
	private boolean tienejugador3;
	private boolean tienejugador4;
	private int separacionWidth;
	private int separacionHeight;
	private int numjugcontiene;

	public Casilla() {
		super();
		this.tema = "";
		this.tipo = 0;
		this.setOpaque(false);
		this.setContentAreaFilled(false);
		this.setBorderPainted(false);
		this.colindantes = new ArrayList<Casilla>();
		this.tienejugador1 = false;
		this.tienejugador2 = false;
		this.tienejugador3 = false;
		this.tienejugador4 = false;
		this.numjugcontiene = 0;
		this.separacionWidth = 0;
		this.separacionHeight = 0;
		this.indice = 0;
	}

	public Casilla(String tema, int tipo, int ind) {
		super();
		this.tema = tema;
		this.tipo = tipo;
		this.setOpaque(false);
		this.setContentAreaFilled(false);
		this.setBorderPainted(false);
		this.colindantes = new ArrayList<Casilla>();
		this.tienejugador1 = false;
		this.tienejugador2 = false;
		this.tienejugador3 = false;
		this.tienejugador4 = false;
		this.numjugcontiene = 0;
		this.separacionWidth = 0;
		this.separacionHeight = 0;
		this.indice = ind;
	}

	public String getTema() {
		return tema;
	}

	public ArrayList<Casilla> getColindantes() {
		return colindantes;
	}

	public void addcolindantes(Casilla... listcasillas) {
		for (Casilla cas : listcasillas) {
			colindantes.add(cas);
		}
	}

	public int getTipo() {
		return this.tipo;
	}

	public boolean getTienejugador1() {
		return this.tienejugador1;
	}

	public void clearTienejugador1() {
		this.tienejugador1 = false;
		this.numjugcontiene--;

	}

	public void setTienejugador1() {
		this.tienejugador1 = true;
		this.numjugcontiene++;
	}

	public boolean getTienejugador2() {
		return this.tienejugador2;
	}

	public void clearTienejugador2() {
		this.tienejugador2 = false;
		this.numjugcontiene--;
	}

	public void setTienejugador2() {
		this.tienejugador2 = true;
		this.numjugcontiene++;
	}

	public boolean getTienejugador3() {
		return this.tienejugador3;
	}

	public void clearTienejugador3() {
		this.tienejugador3 = false;
		this.numjugcontiene--;
	}

	public void setTienejugador3() {
		this.tienejugador3 = true;
		this.numjugcontiene++;
	}

	public boolean getTienejugador4() {
		return this.tienejugador4;
	}

	public void clearTienejugador4() {
		this.tienejugador4 = false;
		this.numjugcontiene--;
	}

	public void setTienejugador4() {
		this.tienejugador4 = true;
		this.numjugcontiene++;
	}

	public int getIndice() {
		return this.indice;
	}

	public int getNumJugContiene() {
		return this.numjugcontiene;
	}

	public void setSeparacionWidth(int Width) {
		this.separacionWidth = Width;
	}

	public int getSeparacionWidth() {
		return this.separacionWidth;
	}

	public void setSeparacionHeight(int Height) {
		this.separacionHeight = Height;
	}

	public int getSeparacionHeight() {
		return this.separacionHeight;
	}
}
