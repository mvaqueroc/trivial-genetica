//import java.util.ArrayList;

public class Jugador {

	private String nombre;
	private boolean quesitoAzul;
	private boolean quesitoRojo;
	private boolean quesitoAmarillo;
	private boolean quesitoVerde;
	private Ficha ficha;
	//private static ArrayList<Boolean> preguntasacertadas = new ArrayList<Boolean>();


	public Jugador() {
		this.nombre = "";
		this.quesitoAzul = false;
		this.quesitoRojo = false;
		this.quesitoAmarillo = false;
		this.quesitoVerde = false;
	}

	public Jugador(String nom) {
		this.nombre = nom;
		this.quesitoAzul = false;
		this.quesitoRojo = false;
		this.quesitoAmarillo = false;
		this.quesitoVerde = false;
	}

	public void clearQuesitoAzul() {
		this.quesitoAzul = false;
	}

	public void setQuesitoAzul() {
		this.quesitoAzul = true;
	}

	public boolean getQuesitoAzul() {
		return this.quesitoAzul;
	}

	public void clearQuesitoRojo() {
		this.quesitoRojo = false;
	}

	public void setQuesitoRojo() {
		this.quesitoRojo = true;
	}

	public boolean getQuesitoRojo() {
		return this.quesitoRojo;
	}

	public void clearQuesitoAmarillo() {
		this.quesitoAmarillo = false;
	}

	public void setQuesitoAmarillo() {
		this.quesitoAmarillo = true;
	}

	public boolean getQuesitoAmarillo() {
		return this.quesitoAmarillo;
	}

	public void clearQuesitoVerde() {
		this.quesitoVerde = false;
	}

	public void setQuesitoVerde() {
		this.quesitoVerde = true;
	}

	public boolean getQuesitoVerde() {
		return this.quesitoVerde;
	}

	public String getNombre() {
		return nombre;
	}

	public Ficha getFicha() {
		return ficha;
	}

	public void setFicha(Ficha ficha) {
		this.ficha = ficha;
	}
	
	public boolean comprobarSiAcertado(){
		return false;
	}
}
