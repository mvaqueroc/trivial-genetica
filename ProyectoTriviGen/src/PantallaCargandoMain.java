
import sun.applet.Main;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.UIManager;

@SuppressWarnings("unused")
public class PantallaCargandoMain {

	cargando screen;

	public PantallaCargandoMain() {
		inicioPantalla();
		screen.velocidadDeCarga();
	}

	private void inicioPantalla() {

		ImageIcon myImage = new ImageIcon(getClass().getResource("/dado/LogoInicioPrograma.png"));
		screen = new cargando(myImage);
		screen.setLocationRelativeTo(null);
		screen.setProgresoMax(100);
		screen.setVisible(true);
		screen.setBackground(new Color(0, 0, 0, 0));

	}

	public static void main(String[] args) 
	{
		
		
		
		
		
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}

		Inicio inicio = new Inicio();

		new PantallaCargandoMain();

		inicio.setVisible(true);
		inicio.setLocationRelativeTo(null);

	}
}