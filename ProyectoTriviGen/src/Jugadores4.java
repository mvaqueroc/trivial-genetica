import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class Jugadores4 extends JFrame {

	private static final int MAX_CHAR = 25;
	private JPanel contentPane;
	private JTextField textField_player1;
	public Inicio frameinicio;
	private JTextField textField_player2;
	private JTextField textField_player3;
	private JTextField textField_player4;

	/**
	 * Create the frame.
	 */
	public Jugadores4() {

		inicioComponentes();
	}

	public Jugadores4(Inicio ini) {
		this.frameinicio = ini;
		inicioComponentes();
	}

	private void inicioComponentes() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 261);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[65.00][grow][162.00,grow][120.00][]",
				"[32px:n:32px][32px][32px][32px][32px][20px:n:20px][32px]"));

		JLabel lblPlayer1 = new JLabel("Jugador 1");
		lblPlayer1.setFont(new Font("Georgia", Font.PLAIN, 13));
		contentPane.add(lblPlayer1, "cell 0 1,alignx trailing");

		textField_player1 = new JTextField();
		textField_player1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (textField_player1.getText().length() > MAX_CHAR) {
					e.consume();
				}
			}
		});
		contentPane.add(textField_player1, "cell 1 1 3 1,growx");
		textField_player1.setColumns(10);

		JLabel lblPlayer2 = new JLabel("Jugador 2");
		lblPlayer2.setFont(new Font("Georgia", Font.PLAIN, 13));
		contentPane.add(lblPlayer2, "cell 0 2,alignx trailing");

		textField_player2 = new JTextField();
		textField_player2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (textField_player2.getText().length() > MAX_CHAR) {
					e.consume();
				}
			}
		});
		contentPane.add(textField_player2, "cell 1 2 3 1,growx");
		textField_player2.setColumns(10);

		JLabel lblPlayer3 = new JLabel("Jugador 3");
		lblPlayer3.setFont(new Font("Georgia", Font.PLAIN, 13));
		contentPane.add(lblPlayer3, "cell 0 3,alignx trailing");

		textField_player3 = new JTextField();
		textField_player3.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (textField_player3.getText().length() > MAX_CHAR) {
					e.consume();
				}
			}
		});
		textField_player3.setColumns(10);
		contentPane.add(textField_player3, "cell 1 3 3 1,growx");

		JLabel lblPlayer4 = new JLabel("Jugador 4");
		lblPlayer4.setFont(new Font("Georgia", Font.PLAIN, 13));
		contentPane.add(lblPlayer4, "cell 0 4,alignx trailing");

		textField_player4 = new JTextField();
		textField_player4.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (textField_player4.getText().length() > MAX_CHAR) {
					e.consume();
				}
			}
		});
		textField_player4.setColumns(10);
		contentPane.add(textField_player4, "cell 1 4 3 1,growx");

		JButton btnBack = new JButton("Atras");
		btnBack.setFont(new Font("Georgia", Font.PLAIN, 13));
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frameinicio.setVisible(true);
				dispose();
			}
		});
		contentPane.add(btnBack, "cell 1 6");

		JButton btnPlay = new JButton("Jugar");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Tablero pantablero = new Tablero(frameinicio, textField_player1.getText(), textField_player2.getText(),
						textField_player3.getText(), textField_player4.getText());
				pantablero.setVisible(true);
				pantablero.setLocationRelativeTo(null);
				dispose();
			}
		});
		btnPlay.setFont(new Font("Georgia", Font.PLAIN, 13));
		contentPane.add(btnPlay, "cell 3 6");
	}

}
