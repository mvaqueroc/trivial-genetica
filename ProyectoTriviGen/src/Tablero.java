
import java.awt.EventQueue;
import java.awt.Image;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;

@SuppressWarnings("serial")
public class Tablero extends JFrame {

	private static JPanel contentPane;
	private Inicio frameinicio;
	private JButton dado;
	public static boolean tirar = true;
	public static boolean semueve = true;
	private static Jugador jugador1;
	private static Jugador jugador2;
	private static Jugador jugador3;
	private static Jugador jugador4;
	private Ficha jug1;
	private Ficha jug2;
	private Ficha jug3;
	private Ficha jug4;
	private static JLabel turnoJugador;
	private JLabel table;
	private Casilla lugaractual;
	private static int numjugadores;
	public static int jugadoractual;
	private ArrayList<Integer> aciertos;
	private ArrayList<Casilla> tablero;
	private ArrayList<Casilla> casDestino;
	private ArrayList<JLabel> flechas;
	private static final String tema1 = "Herencia y Reproduccion";
	private static final String tema2 = "Conceptos Generales";
	private static final String tema3 = "Enfermedades y Terapias";
	private static final String tema4 = "Aplicaciones de la Genetica";
	private static final int tipo1 = 1; // casillas normales
	private static final int tipo2 = 2; // casillas quesitos
	private static final int tipo3 = 3; // casillas dado
	private static final int tipo4 = 4; // casilla central
	public static int contadorFinal = 0;
	public static int contadorAciertos = 0;

	private Casilla Cas1;
	private Casilla Cas2;
	private Casilla Cas3;
	private Casilla Cas4;
	private Casilla Cas5;
	private Casilla Cas6;
	private Casilla Cas7;
	private Casilla Cas8;
	private Casilla Cas9;
	private Casilla Cas10;
	private Casilla Cas11;
	private Casilla Cas12;
	private Casilla Cas13;
	private Casilla Cas14;
	private Casilla Cas15;
	private Casilla Cas16;
	private Casilla Cas17;
	private Casilla Cas18;
	private Casilla Cas19;
	private Casilla Cas20;
	private Casilla Cas21;
	private Casilla Cas22;
	private Casilla Cas23;
	private Casilla Cas24;
	private Casilla Cas25;
	private Casilla Cas26;
	private Casilla Cas27;
	private Casilla Cas28;
	private Casilla Cas29;
	private Casilla Cas30;
	private Casilla Cas31;
	private Casilla Cas32;
	private Casilla Cas33;
	private Casilla Cas34;
	private Casilla Cas35;
	private Casilla Cas36;
	private Casilla Cas37;
	private Casilla Cas38;
	private Casilla Cas39;
	private Casilla Cas40;
	private Casilla Cas41;
	private Casilla Cas42;
	private Casilla Cas43;
	private Casilla Cas44;
	private Casilla Cas45;
	private Casilla Cas46;
	private Casilla Cas47;
	private Casilla Cas48;
	private Casilla Cas49;
	private Casilla Cas50;
	private Casilla Cas51;
	private Casilla Cas52;
	private Casilla Cas53;
	private Casilla Cas54;
	private Casilla Cas55;
	private Casilla Cas56;
	private Casilla Cas57;
	private Casilla Cas58;
	private Casilla Cas59;
	private Casilla Cas60;
	private Casilla Cas61;
	public static JLabel quesitoAzul;
	public static JLabel quesitoRojo;
	public static JLabel quesitoVerde;
	public static JLabel quesitoAmarillo;
	private static JLabel lblTurnoFicha;
	public VentanaFinal pagfinal;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tablero frame = new Tablero();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		});
	}

	/**
	 * Create the frame.
	 */
	public Tablero() {
		jugador1 = new Jugador("Prueba");
		numjugadores = 1;
		jugadoractual = 1;
		inicioTablero();
	}

	public Tablero(Inicio frame, String juga1) {
		this.frameinicio = frame;
		jugador1 = new Jugador(juga1);
		numjugadores = 1;
		jugadoractual = 1;
		inicioTablero();
	}

	public Tablero(Inicio frame, String juga1, String juga2) {
		this.frameinicio = frame;
		jugador1 = new Jugador(juga1);
		jugador2 = new Jugador(juga2);
		numjugadores = 2;
		jugadoractual = 1;
		inicioTablero();
	}

	public Tablero(Inicio frame, String juga1, String juga2, String juga3) {
		this.frameinicio = frame;
		jugador1 = new Jugador(juga1);
		jugador2 = new Jugador(juga2);
		jugador3 = new Jugador(juga3);
		numjugadores = 3;
		jugadoractual = 1;
		inicioTablero();
	}

	public Tablero(Inicio frame, String juga1, String juga2, String juga3, String juga4) {
		this.frameinicio = frame;
		jugador1 = new Jugador(juga1);
		jugador2 = new Jugador(juga2);
		jugador3 = new Jugador(juga3);
		jugador4 = new Jugador(juga4);
		numjugadores = 4;
		jugadoractual = 1;
		inicioTablero();
	}

	public void inicioTablero() {

		setResizable(false);
		setForeground(Color.WHITE);
		setBackground(Color.WHITE);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 833, 780);
		contentPane = new JPanel();
		contentPane.setLayout(null);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		aciertos = new ArrayList<>();
		tablero = new ArrayList<>();
		casDestino = new ArrayList<>();
		flechas = new ArrayList<>();

		dado = new JButton("");
		dado.setBounds(687, 21, 106, 102);
		ponImagenBoton("dado/dadomini.jpg", dado);
		dado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int tirada = 0;
				if (tirar == true) {
					tirada = TiraDado();
					switch (tirada) {
					case 1: {
						ponImagenBoton("dado/cara1.png", dado);
						break;
					}
					case 2: {
						ponImagenBoton("dado/cara2.png", dado);
						break;
					}
					case 3: {
						ponImagenBoton("dado/cara3.png", dado);
						break;
					}
					case 4: {
						ponImagenBoton("dado/cara4.png", dado);
						break;
					}
					case 5: {
						ponImagenBoton("dado/cara5.png", dado);
						break;
					}
					case 6: {
						ponImagenBoton("dado/cara6.png", dado);
						break;
					}
					}
					tirar = false;
					lugaractual = dondeesta(jugadoractual);
					casDestino.clear();
					aciertos.clear();
					casillasDestino(lugaractual, tirada, lugaractual);
					mostrarposibles();
					semueve = true;
				}
			}
		});

		contentPane.add(dado);

		turnoJugador = new JLabel(jugador1.getNombre());
		turnoJugador.setDisplayedMnemonic(KeyEvent.VK_PREVIOUS_CANDIDATE);
		turnoJugador.setFont(new Font("Georgia", Font.PLAIN, 13));
		turnoJugador.setBounds(722, 138, 217, 16);
		contentPane.add(turnoJugador);

		quesitoAzul = new JLabel("");
		quesitoAzul.setBackground(new Color(85, 159, 255));
		quesitoAzul.setBounds(707, 196, 40, 40);
		contentPane.add(quesitoAzul);

		quesitoRojo = new JLabel("");
		quesitoRojo.setBackground(new Color(255, 63, 85));
		quesitoRojo.setBounds(747, 196, 40, 40);
		contentPane.add(quesitoRojo);

		quesitoAmarillo = new JLabel("");
		quesitoAmarillo.setBackground(new Color(255, 255, 85));
		quesitoAmarillo.setBounds(747, 236, 40, 40);
		contentPane.add(quesitoAmarillo);

		quesitoVerde = new JLabel("");
		quesitoVerde.setBackground(new Color(85, 255, 170));
		quesitoVerde.setBounds(707, 236, 40, 40);
		contentPane.add(quesitoVerde);

		cargacasillas();

		cargafichas(numjugadores);

		iniciocolindantes();

		dibujatablero();
		
		lblTurnoFicha.setIcon(jugador1.getFicha().getIcon());
		
		JButton btnNewButton = new JButton("Made by: XY x3  XX x2");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jugador1.setQuesitoAzul();
				Tablero.quesitoAzul.setOpaque(true);
				contentPane.repaint();
				jugador1.setQuesitoRojo();
				Tablero.quesitoRojo.setOpaque(true);
				contentPane.repaint();
				jugador1.setQuesitoAmarillo();
				Tablero.quesitoAmarillo.setOpaque(true);
				contentPane.repaint();
				jugador1.setQuesitoVerde();
				Tablero.quesitoVerde.setOpaque(true);
				contentPane.repaint();
				
				VentanaFinal ventfinal = new VentanaFinal(0, jugador1, frameinicio,
						Tablero.this);
				ventfinal.setVisible(true);
				ventfinal.setLocationRelativeTo(null);
			}
		});
		btnNewButton.setOpaque(false);
		btnNewButton.setContentAreaFilled(false);
		btnNewButton.setBorderPainted(false);
		btnNewButton.setBounds(1, 722, 214, 29);
		contentPane.add(btnNewButton);

		movimientoJugador();

	}

	private void instanciaJugador1() {
		jug1 = new Ficha("fichaLila.png");	
		jugador1.setFicha(jug1);
		jug1.setBounds(Cas46.getSeparacionWidth() + Cas46.getX(), Cas46.getSeparacionHeight() + Cas46.getY(),
				jug1.getWidth(), jug1.getHeight());
		contentPane.add(jug1);
		Cas46.setTienejugador1();
	}

	private void instanciaJugador2() {
		jug2 = new Ficha("fichaNaranja.png");
		jugador2.setFicha(jug2);
		jug2.setBounds(Cas46.getSeparacionWidth() + Cas46.getX() + jug2.getWidth(),
				Cas46.getSeparacionHeight() + Cas46.getY(), jug2.getWidth(), jug2.getHeight());
		contentPane.add(jug2);
		Cas46.setTienejugador2();
	}

	private void instanciaJugador3() {
		jug3 = new Ficha("fichaGris.png");
		jugador3.setFicha(jug3);
		jug3.setBounds(Cas46.getSeparacionWidth() + Cas46.getX(),
				Cas46.getSeparacionHeight() + Cas46.getY() + jug3.getHeight(), jug3.getWidth(), jug3.getHeight());
		contentPane.add(jug3);
		Cas46.setTienejugador3();
	}

	private void instanciaJugador4() {
		jug4 = new Ficha("fichaNegra.png");
		jugador4.setFicha(jug4);
		jug4.setBounds(Cas46.getSeparacionWidth() + Cas46.getX() + jug4.getWidth(),
				10 + Cas46.getY() + jug4.getHeight(), jug4.getWidth(), jug4.getHeight());
		contentPane.add(jug4);
		Cas46.setTienejugador4();
	}

	private void cargafichas(int numjuga) {
		instanciaJugador1();

		if (numjuga >= 2) {
			instanciaJugador2();
		}

		if (numjuga >= 3) {
			instanciaJugador3();
		}

		if (numjuga >= 4) {
			instanciaJugador4();
		}

	}

	private void dibujatablero() {

		table = new JLabel("");
		table.setIcon(new ImageIcon(Tablero.class.getResource("/dado/TableroFinal.png")));
		table.setBounds(30, 21, 668, 780);
		contentPane.add(table);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Tablero.class.getResource("/dado/LogoTriviGen.png")));
		lblNewLabel.setBounds(24, 21, 203, 149);
		contentPane.add(lblNewLabel);
		
		lblTurnoFicha = new JLabel("");
		lblTurnoFicha.setBounds(682, 134, 25, 25);
		contentPane.add(lblTurnoFicha);
	}

	private void cargacasillas() {
		Cas1 = new Casilla(tema3, tipo2, 1);
		tablero.add(Cas1);
		Cas1.setBounds(316, 102, 74, 52);
		contentPane.add(Cas1);

		Cas41 = new Casilla(tema1, tipo1, 41);
		tablero.add(Cas41);
		Cas41.setBounds(320, 156, 70, 45);
		contentPane.add(Cas41);

		Cas38 = new Casilla(tema3, tipo1, 38);
		tablero.add(Cas38);
		Cas38.setBounds(197, 137, 30, 50);
		contentPane.add(Cas38);

		Cas57 = new Casilla(tema1, tipo1, 57);
		tablero.add(Cas57);
		Cas57.setBounds(395, 377, 41, 67);
		contentPane.add(Cas57);

		Cas58 = new Casilla(tema2, tipo1, 58);
		tablero.add(Cas58);
		Cas58.setBounds(440, 377, 41, 67);
		contentPane.add(Cas58);

		Cas59 = new Casilla("", tipo3, 59);
		tablero.add(Cas59);
		Cas59.setBounds(485, 377, 41, 67);
		contentPane.add(Cas59);

		Cas60 = new Casilla(tema3, tipo1, 60);
		tablero.add(Cas60);
		Cas60.setBounds(532, 377, 41, 67);
		contentPane.add(Cas60);

		Cas61 = new Casilla(tema4, tipo1, 61);
		tablero.add(Cas61);
		Cas61.setBounds(578, 377, 41, 67);
		contentPane.add(Cas61);

		Cas52 = new Casilla(tema2, tipo1, 52);
		tablero.add(Cas52);
		Cas52.setBounds(91, 377, 41, 67);
		contentPane.add(Cas52);

		Cas53 = new Casilla(tema1, tipo1, 53);
		tablero.add(Cas53);
		Cas53.setBounds(136, 377, 41, 67);
		contentPane.add(Cas53);

		Cas54 = new Casilla("", tipo3, 54);
		tablero.add(Cas54);
		Cas54.setBounds(183, 377, 41, 67);
		contentPane.add(Cas54);

		Cas55 = new Casilla(tema3, tipo1, 55);
		tablero.add(Cas55);
		Cas55.setBounds(227, 377, 41, 67);
		contentPane.add(Cas55);

		Cas56 = new Casilla(tema4, tipo1, 56);
		tablero.add(Cas56);
		Cas56.setBounds(273, 377, 41, 67);
		contentPane.add(Cas56);

		Cas42 = new Casilla(tema2, tipo1, 42);
		tablero.add(Cas42);
		Cas42.setBounds(320, 201, 70, 45);
		contentPane.add(Cas42);

		Cas43 = new Casilla("", tipo3, 43);
		tablero.add(Cas43);
		Cas43.setBounds(320, 245, 70, 44);
		contentPane.add(Cas43);

		Cas44 = new Casilla(tema4, tipo1, 44);
		tablero.add(Cas44);
		Cas44.setBounds(320, 287, 70, 45);
		contentPane.add(Cas44);

		Cas45 = new Casilla(tema3, tipo1, 45);
		tablero.add(Cas45);
		Cas45.setBounds(320, 332, 70, 44);
		contentPane.add(Cas45);

		Cas47 = new Casilla(tema2, tipo1, 47);
		tablero.add(Cas47);
		Cas47.setBounds(320, 449, 70, 41);
		contentPane.add(Cas47);

		Cas48 = new Casilla(tema1, tipo1, 48);
		tablero.add(Cas48);
		Cas48.setBounds(320, 492, 70, 42);
		contentPane.add(Cas48);

		Cas49 = new Casilla("", tipo3, 49);
		tablero.add(Cas49);
		Cas49.setBounds(320, 537, 70, 40);
		contentPane.add(Cas49);

		Cas50 = new Casilla(tema4, tipo1, 50);
		tablero.add(Cas50);
		Cas50.setBounds(320, 580, 70, 41);
		contentPane.add(Cas50);

		Cas51 = new Casilla(tema3, tipo1, 51);
		tablero.add(Cas51);
		Cas51.setBounds(320, 625, 70, 39);
		contentPane.add(Cas51);

		Cas11 = new Casilla(tema2, tipo2, 11);
		tablero.add(Cas11);
		Cas11.setBounds(626, 377, 48, 70);
		contentPane.add(Cas11);

		Cas31 = new Casilla(tema4, tipo2, 31);
		tablero.add(Cas31);
		Cas31.setBounds(35, 377, 50, 70);
		contentPane.add(Cas31);

		Cas21 = new Casilla(tema1, tipo2, 21);
		tablero.add(Cas21);
		Cas21.setBounds(316, 670, 76, 50);
		contentPane.add(Cas21);

		Cas39 = new Casilla(tema4, tipo1, 39);
		tablero.add(Cas39);
		Cas39.setBounds(234, 120, 33, 50);
		contentPane.add(Cas39);

		Cas40 = new Casilla(tema1, tipo1, 40);
		tablero.add(Cas40);
		Cas40.setBounds(274, 107, 36, 52);
		contentPane.add(Cas40);

		Cas2 = new Casilla(tema1, tipo1, 2);
		tablero.add(Cas2);
		Cas2.setBounds(403, 110, 30, 45);
		contentPane.add(Cas2);

		Cas3 = new Casilla(tema4, tipo1, 3);
		tablero.add(Cas3);
		Cas3.setBounds(445, 120, 30, 44);
		contentPane.add(Cas3);

		Cas4 = new Casilla(tema3, tipo1, 4);
		tablero.add(Cas4);
		Cas4.setBounds(485, 138, 29, 44);
		contentPane.add(Cas4);

		Cas18 = new Casilla(tema1, tipo1, 18);
		tablero.add(Cas18);
		Cas18.setBounds(483, 642, 30, 40);
		contentPane.add(Cas18);

		Cas24 = new Casilla(tema1, tipo1, 24);
		tablero.add(Cas24);
		Cas24.setBounds(197, 638, 28, 53);
		contentPane.add(Cas24);

		Cas23 = new Casilla(tema4, tipo1, 23);
		tablero.add(Cas23);
		Cas23.setBounds(236, 654, 30, 53);
		contentPane.add(Cas23);

		Cas22 = new Casilla(tema3, tipo1, 22);
		tablero.add(Cas22);
		Cas22.setBounds(274, 665, 36, 50);
		contentPane.add(Cas22);

		Cas20 = new Casilla(tema3, tipo1, 20);
		tablero.add(Cas20);
		Cas20.setBounds(403, 667, 30, 50);
		contentPane.add(Cas20);

		Cas19 = new Casilla(tema4, tipo1, 19);
		tablero.add(Cas19);
		Cas19.setBounds(445, 658, 28, 44);
		contentPane.add(Cas19);

		Cas32 = new Casilla(tema2, tipo1, 32);
		tablero.add(Cas32);
		Cas32.setBounds(36, 334, 55, 36);
		contentPane.add(Cas32);

		Cas33 = new Casilla(tema1, tipo1, 33);
		tablero.add(Cas33);
		Cas33.setBounds(50, 294, 48, 38);
		contentPane.add(Cas33);

		Cas34 = new Casilla(tema3, tipo1, 34);
		tablero.add(Cas34);
		Cas34.setBounds(64, 259, 55, 33);
		contentPane.add(Cas34);

		Cas8 = new Casilla(tema3, tipo1, 8);
		tablero.add(Cas8);
		Cas8.setBounds(600, 259, 35, 35);
		contentPane.add(Cas8);

		Cas9 = new Casilla(tema1, tipo1, 9);
		tablero.add(Cas9);
		Cas9.setBounds(614, 297, 44, 30);
		contentPane.add(Cas9);

		Cas10 = new Casilla(tema4, tipo1, 10);
		tablero.add(Cas10);
		Cas10.setBounds(624, 336, 46, 32);
		contentPane.add(Cas10);

		Cas30 = new Casilla(tema2, tipo1, 30);
		tablero.add(Cas30);
		Cas30.setBounds(35, 454, 55, 36);
		contentPane.add(Cas30);

		Cas12 = new Casilla(tema4, tipo1, 12);
		tablero.add(Cas12);
		Cas12.setBounds(624, 455, 48, 30);
		contentPane.add(Cas12);

		Cas29 = new Casilla(tema1, tipo1, 29);
		tablero.add(Cas29);
		Cas29.setBounds(48, 494, 52, 32);
		contentPane.add(Cas29);

		Cas13 = new Casilla(tema1, tipo1, 13);
		tablero.add(Cas13);
		Cas13.setBounds(616, 493, 40, 33);
		contentPane.add(Cas13);

		Cas28 = new Casilla(tema3, tipo1, 28);
		tablero.add(Cas28);
		Cas28.setBounds(62, 532, 55, 33);
		contentPane.add(Cas28);

		Cas14 = new Casilla(tema3, tipo1, 14);
		tablero.add(Cas14);
		Cas14.setBounds(600, 530, 40, 34);
		contentPane.add(Cas14);

		Cas27 = new Casilla(tema4, tipo1, 27);
		tablero.add(Cas27);
		Cas27.setBounds(91, 566, 41, 38);
		contentPane.add(Cas27);

		Cas15 = new Casilla(tema2, tipo1, 15);
		tablero.add(Cas15);
		Cas15.setBounds(578, 566, 40, 32);
		contentPane.add(Cas15);

		Cas35 = new Casilla(tema4, tipo1, 35);
		tablero.add(Cas35);
		Cas35.setBounds(100, 223, 35, 39);
		contentPane.add(Cas35);

		Cas7 = new Casilla(tema2, tipo1, 7);
		tablero.add(Cas7);
		Cas7.setBounds(578, 222, 38, 37);
		contentPane.add(Cas7);

		Cas5 = new Casilla(tema2, tipo1, 5);
		tablero.add(Cas5);
		Cas5.setBounds(520, 162, 29, 38);
		contentPane.add(Cas5);

		Cas37 = new Casilla(tema2, tipo1, 37);
		tablero.add(Cas37);
		Cas37.setBounds(160, 158, 29, 50);
		contentPane.add(Cas37);

		Cas25 = new Casilla(tema2, tipo1, 25);
		tablero.add(Cas25);
		Cas25.setBounds(158, 619, 37, 40);
		contentPane.add(Cas25);

		Cas17 = new Casilla(tema2, tipo1, 17);
		tablero.add(Cas17);
		Cas17.setBounds(515, 620, 36, 38);
		contentPane.add(Cas17);

		Cas36 = new Casilla("", tipo3, 36);
		tablero.add(Cas36);
		Cas36.setBounds(126, 191, 35, 40);
		contentPane.add(Cas36);

		Cas6 = new Casilla("", tipo3, 6);
		tablero.add(Cas6);
		Cas6.setBounds(552, 193, 38, 30);
		contentPane.add(Cas6);

		Cas16 = new Casilla("", tipo3, 16);
		tablero.add(Cas16);
		Cas16.setBounds(552, 595, 36, 38);
		contentPane.add(Cas16);

		Cas26 = new Casilla("", tipo3, 26);
		tablero.add(Cas26);
		Cas26.setBounds(123, 598, 38, 30);
		contentPane.add(Cas26);

		Cas46 = new Casilla("", tipo4, 46);
		tablero.add(Cas46);
		Cas46.setBounds(320, 377, 70, 67);
		Cas46.setSeparacionWidth(10);
		Cas46.setSeparacionHeight(10);
		contentPane.add(Cas46);
	}

	private void iniciocolindantes() {
		Cas1.addcolindantes(Cas2, Cas40, Cas41);
		Cas2.addcolindantes(Cas1, Cas3);
		Cas3.addcolindantes(Cas2, Cas4);
		Cas4.addcolindantes(Cas3, Cas5);
		Cas5.addcolindantes(Cas4, Cas6);
		Cas6.addcolindantes(Cas5, Cas7);
		Cas7.addcolindantes(Cas6, Cas8);
		Cas8.addcolindantes(Cas7, Cas9);
		Cas9.addcolindantes(Cas8, Cas10);
		Cas10.addcolindantes(Cas9, Cas11);
		Cas11.addcolindantes(Cas10, Cas12, Cas61);
		Cas12.addcolindantes(Cas11, Cas13);
		Cas13.addcolindantes(Cas12, Cas14);
		Cas14.addcolindantes(Cas13, Cas15);
		Cas15.addcolindantes(Cas14, Cas16);
		Cas16.addcolindantes(Cas15, Cas17);
		Cas17.addcolindantes(Cas16, Cas18);
		Cas18.addcolindantes(Cas17, Cas19);
		Cas19.addcolindantes(Cas18, Cas20);
		Cas20.addcolindantes(Cas19, Cas21);
		Cas21.addcolindantes(Cas20, Cas22, Cas51);
		Cas22.addcolindantes(Cas21, Cas23);
		Cas23.addcolindantes(Cas22, Cas24);
		Cas24.addcolindantes(Cas23, Cas25);
		Cas25.addcolindantes(Cas24, Cas26);
		Cas26.addcolindantes(Cas25, Cas27);
		Cas27.addcolindantes(Cas26, Cas28);
		Cas28.addcolindantes(Cas27, Cas29);
		Cas29.addcolindantes(Cas28, Cas30);
		Cas30.addcolindantes(Cas29, Cas31);
		Cas31.addcolindantes(Cas30, Cas32, Cas52);
		Cas32.addcolindantes(Cas31, Cas33);
		Cas33.addcolindantes(Cas32, Cas34);
		Cas34.addcolindantes(Cas33, Cas35);
		Cas35.addcolindantes(Cas34, Cas36);
		Cas36.addcolindantes(Cas35, Cas37);
		Cas37.addcolindantes(Cas36, Cas38);
		Cas38.addcolindantes(Cas37, Cas39);
		Cas39.addcolindantes(Cas38, Cas40);
		Cas40.addcolindantes(Cas39, Cas1);
		Cas41.addcolindantes(Cas1, Cas42);
		Cas42.addcolindantes(Cas41, Cas43);
		Cas43.addcolindantes(Cas42, Cas44);
		Cas44.addcolindantes(Cas43, Cas45);
		Cas45.addcolindantes(Cas44, Cas46);
		Cas46.addcolindantes(Cas45, Cas47, Cas56, Cas57);
		Cas47.addcolindantes(Cas46, Cas48);
		Cas48.addcolindantes(Cas47, Cas49);
		Cas49.addcolindantes(Cas48, Cas50);
		Cas50.addcolindantes(Cas49, Cas51);
		Cas51.addcolindantes(Cas50, Cas21);
		Cas52.addcolindantes(Cas31, Cas53);
		Cas53.addcolindantes(Cas52, Cas54);
		Cas54.addcolindantes(Cas53, Cas55);
		Cas55.addcolindantes(Cas54, Cas56);
		Cas56.addcolindantes(Cas55, Cas46);
		Cas57.addcolindantes(Cas46, Cas58);
		Cas58.addcolindantes(Cas57, Cas59);
		Cas59.addcolindantes(Cas58, Cas60);
		Cas60.addcolindantes(Cas59, Cas61);
		Cas61.addcolindantes(Cas60, Cas11);

	}

	public void ponImagenBoton(String imgPath, JButton btn) {
		try {
			ImageIcon img = new ImageIcon(getClass().getResource((imgPath)));
			btn.setIcon(img);
		} catch (Exception ex) {
			System.out.println(ex.getStackTrace());
		}
	}

	public int TiraDado() {
		int tirada;
		Random rnd = new Random();
		tirada = rnd.nextInt(6) + 1;
		return tirada;

	}

	public void casillasDestino(Casilla actual, int paso, Casilla anterior) {
		if (paso > 1) {
			for (Casilla cas : actual.getColindantes()) {
				if (cas != anterior) {
					casillasDestino(cas, paso - 1, actual);
				}
			}
		} else {
			for (Casilla cas : actual.getColindantes()) {
				if (cas != anterior) {
					aciertos.add(cas.getIndice());
					casDestino.add(cas);
				}
			}
		}
	}

	private Casilla dondeesta(int actual) {

		for (Casilla cas : tablero) {
			switch (actual) {
			case 1:
				if (cas.getTienejugador1() == true) {
					lugaractual = cas;
				}
				break;
			case 2:
				if (cas.getTienejugador2() == true) {
					lugaractual = cas;
				}
				break;
			case 3:
				if (cas.getTienejugador3() == true) {
					lugaractual = cas;
				}
				break;
			case 4:
				if (cas.getTienejugador4() == true) {
					lugaractual = cas;
				}
				break;
			}
		}
		return lugaractual;
	}

	private void mostrarposibles() {
		flechas.clear();
		JLabel n;
		contentPane.remove(table);
		for (Casilla cas : casDestino) {

			n = new JLabel();
			n.setIcon(new ImageIcon(Tablero.class.getResource("/dado/flecharoja.png")));
			n.setBounds(cas.getX() + (cas.getWidth() / 2 - 15), cas.getY() + (cas.getHeight() / 2 - 30), 30, 40);
			contentPane.add(n);
			flechas.add(n);

		}
		contentPane.add(table);
		repaint();
	}

	private void borraposibles() {
		for (JLabel jl : flechas) {
			contentPane.remove(jl);
		}
		flechas.clear();
		repaint();
	}

	@SuppressWarnings("deprecation")
	private void segunjugadorescasilla(Jugador ju, Casilla cas) {
		switch (cas.getNumJugContiene()) {
		case 0:
			ju.getFicha().move(cas.getX() + (cas.getWidth() / 2 - ju.getFicha().getWidth()),
					cas.getY() + (cas.getHeight() / 2 - ju.getFicha().getHeight()));
			break;
		case 1:
			ju.getFicha().move(cas.getX() + (cas.getWidth() / 2 - ju.getFicha().getWidth()) + ju.getFicha().getWidth(),
					cas.getY() + (cas.getHeight() / 2 - ju.getFicha().getHeight()));
			break;
		case 2:
			ju.getFicha().move(cas.getX() + (cas.getWidth() / 2 - ju.getFicha().getWidth()),
					cas.getY() + (cas.getHeight() / 2 - ju.getFicha().getHeight()) + ju.getFicha().getHeight());
			break;
		case 3:
			ju.getFicha().move(cas.getX() + (cas.getWidth() / 2 - ju.getFicha().getWidth()) + ju.getFicha().getWidth(),
					cas.getY() + (cas.getHeight() / 2 - ju.getFicha().getHeight()) + ju.getFicha().getHeight());
			break;
		default:
			ju.getFicha().move(cas.getX(), cas.getY());
			break;
		}
	}

	private void movimientoJugador() {
		for (Casilla cas : tablero) {
			cas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (tirar == false && semueve == true) {
						if (casDestino.contains(cas)) {
							switch (jugadoractual) {
							case 1:
								segunjugadorescasilla(jugador1, cas);
								cas.setTienejugador1();
								lugaractual.clearTienejugador1();
								if (cas.getTipo() == 4) {
									if (comprovarquesitos(jugador1)) {
										VentanaFinal ventfinal = new VentanaFinal(0, jugador1, frameinicio,
												Tablero.this);
										ventfinal.setVisible(true);
										ventfinal.setLocationRelativeTo(null);
									} else {
										Tablero.tirar = true;
										Tablero.semueve = true;
									}
								} else {
									abrirventana(cas, jugador1);
								}
								semueve = false;
								borraposibles();
								break;

							case 2:
								segunjugadorescasilla(jugador2, cas);
								cas.setTienejugador2();
								lugaractual.clearTienejugador2();
								if (cas.getTipo() == 4) {
									if (comprovarquesitos(jugador2)) {
										VentanaFinal ventfinal = new VentanaFinal(0, jugador2, frameinicio,
												Tablero.this);
										ventfinal.setVisible(true);
										ventfinal.setLocationRelativeTo(null);
									} else {
										Tablero.tirar = true;
										Tablero.semueve = true;
									}
								} else {
									abrirventana(cas, jugador2);
								}
								semueve = false;
								borraposibles();
								break;

							case 3:
								segunjugadorescasilla(jugador3, cas);
								cas.setTienejugador3();
								lugaractual.clearTienejugador3();
								if (cas.getTipo() == 4) {
									if (comprovarquesitos(jugador3)) {
										VentanaFinal ventfinal = new VentanaFinal(0, jugador3, frameinicio,
												Tablero.this);
										ventfinal.setVisible(true);
										ventfinal.setLocationRelativeTo(null);
									} else {
										Tablero.tirar = true;
										Tablero.semueve = true;
									}
								} else {
									abrirventana(cas, jugador3);
								}
								semueve = false;
								borraposibles();
								break;

							case 4:
								segunjugadorescasilla(jugador4, cas);
								cas.setTienejugador4();
								lugaractual.clearTienejugador4();
								if (cas.getTipo() == 4) {
									if (comprovarquesitos(jugador4)) {
										VentanaFinal ventfinal = new VentanaFinal(0, jugador4, frameinicio,
												Tablero.this);
										ventfinal.setVisible(true);
										ventfinal.setLocationRelativeTo(null);
									} else {
										Tablero.tirar = true;
										Tablero.semueve = true;
									}
								} else {
									abrirventana(cas, jugador4);
								}
								semueve = false;
								borraposibles();
								break;
							}
						}
					}
				}
			});
		}
	}

	public static void pintaQuesito(String tema, Jugador jugadoractual) {
		if (tema.equals(tema2)) {
			jugadoractual.setQuesitoAmarillo();
			Tablero.quesitoAmarillo.setOpaque(true);
			contentPane.repaint();
		}
		if (tema.equals(tema1)) {
			jugadoractual.setQuesitoRojo();
			Tablero.quesitoRojo.setOpaque(true);
			contentPane.repaint();
		}
		if (tema.equals(tema3)) {
			jugadoractual.setQuesitoAzul();
			Tablero.quesitoAzul.setOpaque(true);
			contentPane.repaint();
		}
		if (tema.equals(tema4)) {
			jugadoractual.setQuesitoVerde();
			Tablero.quesitoVerde.setOpaque(true);
			contentPane.repaint();
		}

	}

	public static void actualizaquesito(int numjuga) {
		switch (numjuga) {
		case 1:
			if (Tablero.jugador1.getQuesitoAmarillo() == true) {
				Tablero.quesitoAmarillo.setOpaque(true);
			} else {
				Tablero.quesitoAmarillo.setOpaque(false);
			}
			if (Tablero.jugador1.getQuesitoRojo() == true) {
				Tablero.quesitoRojo.setOpaque(true);
			} else {
				Tablero.quesitoRojo.setOpaque(false);
			}
			if (Tablero.jugador1.getQuesitoAzul() == true) {
				Tablero.quesitoAzul.setOpaque(true);
			} else {
				Tablero.quesitoAzul.setOpaque(false);
			}
			if (Tablero.jugador1.getQuesitoVerde() == true) {
				Tablero.quesitoVerde.setOpaque(true);
			} else {
				Tablero.quesitoVerde.setOpaque(false);
			}
			contentPane.repaint();
			break;
		case 2:
			if (Tablero.jugador2.getQuesitoAmarillo() == true) {
				Tablero.quesitoAmarillo.setOpaque(true);
			} else {
				Tablero.quesitoAmarillo.setOpaque(false);
			}
			if (Tablero.jugador2.getQuesitoRojo() == true) {
				Tablero.quesitoRojo.setOpaque(true);
			} else {
				Tablero.quesitoRojo.setOpaque(false);
			}
			if (Tablero.jugador2.getQuesitoAzul() == true) {
				Tablero.quesitoAzul.setOpaque(true);
			} else {
				Tablero.quesitoAzul.setOpaque(false);
			}
			if (Tablero.jugador2.getQuesitoVerde() == true) {
				Tablero.quesitoVerde.setOpaque(true);
			} else {
				Tablero.quesitoVerde.setOpaque(false);
			}
			contentPane.repaint();
			break;
		case 3:
			if (Tablero.jugador3.getQuesitoAmarillo() == true) {
				Tablero.quesitoAmarillo.setOpaque(true);
			} else {
				Tablero.quesitoAmarillo.setOpaque(false);
			}
			if (Tablero.jugador3.getQuesitoRojo() == true) {
				Tablero.quesitoRojo.setOpaque(true);
			} else {
				Tablero.quesitoRojo.setOpaque(false);
			}
			if (Tablero.jugador3.getQuesitoAzul() == true) {
				Tablero.quesitoAzul.setOpaque(true);
			} else {
				Tablero.quesitoAzul.setOpaque(false);
			}
			if (Tablero.jugador3.getQuesitoVerde() == true) {
				Tablero.quesitoVerde.setOpaque(true);
			} else {
				Tablero.quesitoVerde.setOpaque(false);
			}
			contentPane.repaint();
			break;
		case 4:
			if (Tablero.jugador4.getQuesitoAmarillo() == true) {
				Tablero.quesitoAmarillo.setOpaque(true);
			} else {
				Tablero.quesitoAmarillo.setOpaque(false);
			}
			if (Tablero.jugador4.getQuesitoRojo() == true) {
				Tablero.quesitoRojo.setOpaque(true);
			} else {
				Tablero.quesitoRojo.setOpaque(false);
			}
			if (Tablero.jugador4.getQuesitoAzul() == true) {
				Tablero.quesitoAzul.setOpaque(true);
			} else {
				Tablero.quesitoAzul.setOpaque(false);
			}
			if (Tablero.jugador4.getQuesitoVerde() == true) {
				Tablero.quesitoVerde.setOpaque(true);
			} else {
				Tablero.quesitoVerde.setOpaque(false);
			}
			contentPane.repaint();
			break;
		}
	}

	private void abrirventana(Casilla cas, Jugador ju) {
		if (cas.getTipo() == 3) {
			tirar = true;
			semueve = true;
		} else {
			VentanaPreg ventpreg = new VentanaPreg(cas.getTema(), cas, ju, numjugadores, jugadoractual);
			ventpreg.setVisible(true);
			ventpreg.setLocationRelativeTo(null);
		}
	}

	public static void actualizaturnojugador(int turnojuga) {
		switch (turnojuga) {
		case 1:
			turnoJugador.setText(jugador1.getNombre());
			lblTurnoFicha.setIcon(jugador1.getFicha().getIcon());
			break;
		case 2:
			turnoJugador.setText(jugador2.getNombre());
			lblTurnoFicha.setIcon(jugador2.getFicha().getIcon());
			break;
		case 3:
			turnoJugador.setText(jugador3.getNombre());
			lblTurnoFicha.setIcon(jugador3.getFicha().getIcon());
			break;
		case 4:
			turnoJugador.setText(jugador4.getNombre());
			lblTurnoFicha.setIcon(jugador4.getFicha().getIcon());
			break;
		}
	}

	private boolean comprovarquesitos(Jugador ju) {
		if (ju.getQuesitoAmarillo() == true & ju.getQuesitoAzul() == true & ju.getQuesitoRojo() == true
				& ju.getQuesitoVerde() == true) {
			return true;
		} else {
			return false;
		}
	}

	public static void actualizaturno(boolean acierto) {
		if (numjugadores > 1) {
			if (jugadoractual == numjugadores) {
				if (acierto == false) {
					Tablero.jugadoractual = 1;
					Tablero.tirar = true;
					Tablero.semueve = true;
				} else {
					Tablero.tirar = true;
					Tablero.semueve = true;
				}
			} else {
				if (acierto == false) {
					Tablero.jugadoractual++;

					Tablero.tirar = true;
					Tablero.semueve = true;
				} else {
					Tablero.tirar = true;
					Tablero.semueve = true;
				}
			}
		} else {
			Tablero.tirar = true;
			Tablero.semueve = true;
		}
	}
}
