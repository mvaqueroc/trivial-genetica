
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

@SuppressWarnings("serial")
public class Jugadores1 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private static final int MAX_CHAR = 25;
	public Inicio frameinicio;

	public Jugadores1() {
		setResizable(false);
		inicioComponentes();
	}

	public Jugadores1(Inicio ini) {
		this.frameinicio = ini;
		inicioComponentes();
	}

	private void inicioComponentes() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[65.00][][162.00,grow][120.00][]", "[32][][32px][][32px]"));

		JLabel lblPlayer1 = new JLabel("Jugador 1");
		lblPlayer1.setFont(new Font("Georgia", Font.PLAIN, 11));
		contentPane.add(lblPlayer1, "cell 0 1,alignx trailing");

		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (textField.getText().length() > MAX_CHAR) {
					e.consume();
				}
			}
		});
		textField.setFont(new Font("Georgia", Font.PLAIN, 11));
		contentPane.add(textField, "cell 1 1 3 1,growx");
		textField.setColumns(10);

		JButton btnBack = new JButton("Atr\u00E1s");
		btnBack.setFont(new Font("Georgia", Font.PLAIN, 11));
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frameinicio.setVisible(true);
				dispose();
			}
		});
		contentPane.add(btnBack, "cell 1 3");

		JButton btnPlay = new JButton("Jugar");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Tablero pantablero = new Tablero(frameinicio, textField.getText());
				pantablero.setVisible(true);
				pantablero.setLocationRelativeTo(null);
				dispose();
			}
		});
		btnPlay.setFont(new Font("Georgia", Font.PLAIN, 11));
		contentPane.add(btnPlay, "cell 3 3");
	}

}
