import javax.swing.ImageIcon;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class Ficha extends JLabel {

	private ImageIcon ficha;
	private final String rutaimagen = "/dado/";
	private int width;
	private int height;

	public Ficha(String url) {
		this.ficha = new ImageIcon(Ficha.class.getResource(rutaimagen + url));
		this.setIcon(ficha);
		this.width = 25;
		this.height = 25;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public ImageIcon getImageIcon() {
		return ficha;
	}

}
