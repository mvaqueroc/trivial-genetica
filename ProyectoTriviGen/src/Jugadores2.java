
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.Font;

@SuppressWarnings("serial")
public class Jugadores2 extends JFrame {

	private static final int MAX_CHAR = 25;
	private JPanel contentPane;
	private JTextField textField_jugador1;
	public Inicio frameinicio;
	private JTextField textField_jugador2;

	public Jugadores2() {
		setResizable(false);
		inicioComponentes();
	}

	public Jugadores2(Inicio ini) {
		this.frameinicio = ini;
		inicioComponentes();
	}

	private void inicioComponentes() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane
				.setLayout(new MigLayout("", "[65.00][grow][162.00,grow][120.00][]", "[32][32px][32px][32px][32px]"));

		JLabel lblPlayer1 = new JLabel("Jugador 1");
		lblPlayer1.setFont(new Font("Georgia", Font.PLAIN, 11));
		contentPane.add(lblPlayer1, "cell 0 1,alignx trailing");

		textField_jugador1 = new JTextField();
		textField_jugador1.setFont(new Font("Georgia", Font.PLAIN, 11));
		textField_jugador1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (textField_jugador1.getText().length() > MAX_CHAR) {
					e.consume();
				}
			}
		});
		contentPane.add(textField_jugador1, "cell 1 1 3 1,growx");
		textField_jugador1.setColumns(10);

		JButton btnBack = new JButton("Atras");
		btnBack.setFont(new Font("Georgia", Font.PLAIN, 11));
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frameinicio.setVisible(true);
				dispose();
			}
		});

		JLabel lblPlayer2 = new JLabel("Jugador 2");
		lblPlayer2.setFont(new Font("Georgia", Font.PLAIN, 11));
		contentPane.add(lblPlayer2, "cell 0 2,alignx trailing");

		textField_jugador2 = new JTextField();
		textField_jugador2.setFont(new Font("Georgia", Font.PLAIN, 11));
		textField_jugador2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (textField_jugador2.getText().length() > MAX_CHAR) {
					e.consume();
				}
			}
		});
		contentPane.add(textField_jugador2, "cell 1 2 3 1,growx");
		textField_jugador2.setColumns(10);
		contentPane.add(btnBack, "cell 1 4");

		JButton btnPlay = new JButton("Jugar");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Tablero pantablero = new Tablero(frameinicio, textField_jugador1.getText(),
						textField_jugador2.getText());
				pantablero.setVisible(true);
				pantablero.setLocationRelativeTo(null);
				dispose();
			}
		});
		btnPlay.setFont(new Font("Georgia", Font.PLAIN, 11));
		contentPane.add(btnPlay, "cell 3 4");
	}

}
